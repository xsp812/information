from flask import session, current_app, render_template, g, abort, request, jsonify
from info import constants, db
from info.utils.common import login_user
from info.models import User, News, Comment, CommentLike
from info.utils.response_code import RET
from . import news_dp


@news_dp.route("/comment_like",methods=["post"])
@login_user
def comment_like():
    """评论ID的点赞和取消点赞"""
    """
       # 1.获取参数
      #     1.1获取新闻id ,评论id ,action(点赞，取消点赞)
      # 2.校验参数
      #     2.1 判断参数是否存在 
      #     2.2首先判断用户是否登录
      #     2.3 action in (add , remove)
      # 3.逻辑处理
      #     3.1根据评论id获取评论模型对象（只有评论存在的时候才能点赞或取消点赞）
      #     3.2根据action去完成点赞或取消点赞的功能
      #     3.3 根据user_id 和comment_id 查询CommentLike是否存在
      #         3.3.1CommentLike 存在表示用户点过该条评论
      #         3.3.2不存在 表示 ：新建CommentLike模型对象最终保存会数据库
      #         3.3.3记录comment模型的点赞数量+1
      #     3.4取消点赞：根据user_id 和comment_id 查询CommentLike是否存在
      #        存在才能取消
      # 记录comment模型的点赞数量-1
      # 4.返回值处理
      # 
      """
    #     1.1获取新闻id ,评论id ,action(点赞，取消点赞)
    params_dicr = request.json
    comment_id = params_dicr.get("comment_id")
    news_id = params_dicr.get("news_id")
    action = params_dicr.get("action")
    user = g.user
    #  2.1 判断参数是否存在 news_id
    if not all([news_id, comment_id, action]):
        return jsonify(errno=RET.DATAERR, errmsg="参数不足")
    #     3.1首先判断用户是否登录
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    if action not in ["add", "remove"]:
        return jsonify(errno=RET.PARAMERR, errmsg="action数据错误")

    # 3.逻辑处理
    #     3.1根据评论id获取评论模型对象（只有评论存在的时候才能点赞或取消点赞）
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询评论异常")

    #     3.2根据action去完成点赞或取消点赞的功能

    if action == "add":
        comment_like = CommentLike.query.filter(CommentLike.user_id == user.id,
                                                CommentLike.comment_id == comment_id
                                                ).first()
        if not comment_like:
            # 3.3 根据user_id 和comment_id 查询CommentLike是否存在
            comment_like = CommentLike()
            comment_like.user_id = user.id
            comment_like.comment_id = comment_id
            db.session.add(comment_like)
            comment.like_count += 1

    #     3.3.1CommentLike 存在表示用户点过该条评论
    #         3.3.2不存在 表示 ：新建CommentLike模型对象最终保存会数据库
    #         3.3.3记录comment模型的点赞数量+1
    else:
        comment_like = CommentLike.query.filter(CommentLike.user_id == user.id,
                                                CommentLike.comment_id == comment_id
                                                ).first()
        if comment_like:
            db.session.delete(comment_like)
            comment.like_count -= 1
    #     3.4取消点赞：根据user_id 和comment_id 查询CommentLike是否存在
    #        存在才能取消
    # 记录comment模型的点赞数量-1
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存数据异常")

    return jsonify(errno=RET.OK, errmsg="ok")


@news_dp.route("/add_comment", methods=["POST"])
@login_user
def add_comment():
    "添加评论"
    """
     # 1.获取参数
    #     1.1获取新闻id ,回复内容comment ,回复评论的id
    # 2.校验参数
    #     2.1 判断参数是否存在 news_id comment
    # 3.逻辑处理
    #     3.1首先判断用户是否登录
    #     3.2通过新闻id获取新闻
    #     3.3初始化数据模型 保存到数据库
    #     3.4提交到数据库
    # 4.返回值处理
    # 
    
    
    """
    # 1.获取参数
    #     1.1获取新闻id ,回复内容comment ,回复评论的id
    params_dicr = request.json
    news_id = params_dicr.get("news_id")
    comment_str = params_dicr.get("comment")
    parent_id = params_dicr.get("parent_id")
    user = g.user
    # 2.校验参数
    #     2.1 判断参数是否存在 news_id comment
    if not all([news_id, comment_str]):
        return jsonify(errno=RET.DATAERR, errmsg="参数不足")
    # 3.逻辑处理
    #     3.1首先判断用户是否登录
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    #     3.2通过新闻id获取新闻
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库异常")
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="没有该新闻")
    #     3.3初始化数据模型 保存到数据库
    comment = Comment()
    comment.news_id = news_id
    comment.user_id = user.id
    comment.content = comment_str
    if parent_id:
        comment.parent_id = parent_id

    #     3.4提交到数据库
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存到数据库失败")
    # 4.返回值处理
    return jsonify(errno=RET.OK, errmsg="OK", data=comment.to_dict())


@news_dp.route("/news_collect", methods=["POST"])
@login_user
def news_collect():
    """收藏新闻和取消收藏"""
    """
    # 1.获取参数
    #     1.1获取新闻id ,action值
    # 2.校验参数
    #     2.1 判断参数是否存在 news_id 
    #     2.2 判断action的值是否在（collect,cancel_collect）
    # 3.逻辑处理
    #     3.1首先判断用户是否登录
    #     3.2通过新闻id获取新闻
    #     3.3通过action值对新闻进行collect添加收藏或者cancel_collect取消收藏操作
    #     3.4提交到数据库
    # 4.返回值处理
    # 
    """
    # 1.获取参数
    #     1.1获取新闻id ,action值
    params_dict = request.json
    news_id = params_dict.get("news_id")
    action = params_dict.get("action")
    user = g.user
    # 2.校验参数
    #     2.1 判断参数是否存在 news_id
    if not all([news_id, action]):
        return jsonify(errno=RET.DATAERR, errmsg="参数不足")
    #     2.2 判断action的值是否在（collect,cancel_collect）
    if action not in ("collect", "cancel_collect"):
        return jsonify(errno=RET.PARAMERR, errmsg="action参数错误")

    # 3.逻辑处理
    #     3.1首先判断用户是否登录
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    #     3.2通过新闻id获取新闻
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库错误")
    #     3.3通过action值对新闻进行collect添加收藏或者cancel_collect取消收藏操作
    if action == "collect":
        user.collection_news.append(news)
    else:
        user.collection_news.remove(news)

    #     3.4提交到数据库
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DATAERR, errmsg="保存到数据库失败")
    # 4.返回值处理
    return jsonify(errno=RET.OK, errmsg="OK")


@news_dp.route("/<int:new_id>")
@login_user
def detail(new_id):
    """新闻详情页面展示"""
    # 通过session获取用户ID 然后得到用户信息
    user = g.user

    # -----点击排行
    news_list = None
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    click_news_list = []
    for news in news_list if news_list else []:
        click_news_list.append(news.to_basic_dict())
        # -----显示新闻详情
    try:
        news = News.query.get(new_id)
    except Exception as e:
        current_app.logger.error(e)
        abort(404)
    if not news:
        abort(404)
    news.clicks += 1
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        abort(404)

    # 判断是否收藏过该新闻
    is_collected = False
    if user:
        if news in user.collection_news:
            is_collected = True

    # ---------评论的显示
    comments = []
    try:
        # 查询该条新闻所有的评论
        comments = Comment.query.filter(Comment.news_id == news.id).order_by(Comment.create_time.desc()).all()

    except Exception as e:
        current_app.logger.error(e)

    commentlike_id_list=[]
    if user:
        try:
            # -------查询当前用户在当前新闻的评论里点了那几条评论
            # 查询当前新闻所有的评论，取得所有评论的id
            comment_id_list = [comment.id for comment in comments]
            # 在通过评论点赞咯行 查看当前用户点了几条评论
            comentlike_model=CommentLike.query.filter(CommentLike.comment_id.in_(comment_id_list),
                                     CommentLike.user_id == user.id
                                     ).all()

            commentlike_id_list = [commentlike.comment_id for commentlike in comentlike_model]
        except Exception as e:
            current_app.logger.error(e)

    comments_list = []
    for comment in comments if comments else []:
        comment_dict = comment.to_dict()
        comment_dict["is_like"] =False
        if comment.id in commentlike_id_list:
            comment_dict["is_like"]=True
        comments_list.append(comment_dict)

    data = {
        "user_info": user.to_dict() if user else None,
        "news": news.to_dict(),
        "click_news_list": click_news_list,
        "is_collected": is_collected,
        "comments": comments_list
    }
    return render_template('news/detail.html', data=data)
