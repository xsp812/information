from flask import session, current_app, g
import functools
from info.models import User


def do_index_class(index):
    """过滤器"""
    if index == 0:
        return "first"
    elif index == 1:
        return "second"
    elif index == 2:
        return "third"
    else:
        return ""



def login_user(view_func):
    @functools.wraps(view_func)
    def wrap(*args, **kwargs):
        #  添加需求
        user_id = session.get("user_id")
        user = None  # type:User
        from info.models import User
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        g.user = user
        return view_func(*args, **kwargs)
    return wrap