from flask import Blueprint

# 创建蓝图对象
admin_dp=Blueprint("admin",__name__,url_prefix='/admin')

from .views import *

@admin_dp.before_request
def before_request():
    """拦截普通用户进入后台页面"""
    if not request.url.endswith(url_for("admin.admin_login")):
        user_id = session.get("user_id")
        is_admin = session.get("is_admin",False)

        if not user_id or not is_admin:
            return redirect("/")