from info import db, constants
from info.models import User, Category, News
from info.utils.response_code import RET
from . import  profile_bp
from flask import render_template, g, request, jsonify, session, current_app
from info.utils.common import login_user
from info.utils.image_store import qiniu_image_store

@profile_bp.route('/news_list',methods=["GET","POST"])
@login_user
def news_list():
    """用户新闻列表展示"""
    p = request.args.get("p", 1)
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    user = g.user
    news = []
    current_page = 1
    total_page = 1
    if user:
        try:
            paginate = News.query.filter(News.user_id == user.id).paginate(p, constants.USER_COLLECTION_MAX_NEWS, False)
            # 获取分页内容
            news = paginate.items
            # 获取当前页
            current_page = paginate.page
            # 获取总页数
            total_page = paginate.pages
        except Exception as e:
            current_app.logger.error(e)

    news_dict = []
    for new in news if news else []:
        news_dict.append(new.to_review_dict())

    data = {
        "current_page": current_page,
        "total_page": total_page,
        "news_list": news_dict
    }
    return render_template("news/user_news_list.html", data=data)


@profile_bp.route('/news_release',methods=["GET","POST"])
@login_user
def news_release():
    """发布新闻模块"""
    if request.method=="GET":
        try:
           categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
#         分列对象转化为字典
        category_dict=[]
        for category in categories if categories else []:
            category_dict.append(category.to_dict())
        # 移除最新分类
        category_dict.pop(0)
        data ={
            "categories":category_dict
        }
        return render_template("news/user_news_release.html",data=data)


    # 获取参数
    title = request.form.get("title")
    source = "个人发布"
    digest = request.form.get("digest")
    content = request.form.get("content")
    index_image = request.files.get("index_image")
    category_id = request.form.get("category_id")
    user = g.user
    #参数校验
    if not all([title,source,digest,content,index_image,category_id]):
         return jsonify(errno=RET.PARAMERR, errmsg="参数不足")

    index_image = index_image.read()

    #上传到七牛云
    try:
        image = qiniu_image_store(index_image)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="上传失败")
    # 创建新闻模型对象
    news = News()
    news.category_id=category_id
    news.title = title
    news.content = content
    news.digest = digest
    news.source = source
    news.index_image_url = constants.QINIU_DOMIN_PREFIX +image
    news.status = 1
    news.user_id= user.id
    try:
        db.session.add(news)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="保存到数据库异常")
    return jsonify(errno=RET.OK, errmsg="新闻发布成功")

@profile_bp.route('/collection')
@login_user
def user_collection():
    """收藏新闻显示"""
    p = request.args.get("p",1)
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    user = g.user
    collections = []
    current_page = 1
    total_page = 1
    if user:
        try:
            paginate = user.collection_news.paginate(p, constants.OTHER_NEWS_PAGE_MAX_COUNT, False)
            # 获取分页内容
            collections = paginate.items
            # 获取当前页
            current_page = paginate.page
            # 获取总页数
            total_page = paginate.pages
        except Exception as e:
            current_app.logger.error(e)


    collections_dict =[]
    for new in collections if collections else []:
        collections_dict.append(new.to_basic_dict())

    data={
        "current_page":current_page,
        "total_page":total_page,
        "collections":collections_dict
    }
    return render_template("news/user_collection.html",data = data)



@profile_bp.route("/pass_info",methods=["POST","GET"])
@login_user
def pass_info():
    """密码修改"""
    if request.method == "GET":
        return render_template("news/user_pass_info.html")
    user =g.user
    # 1.获取参数 新密码 和 老密码
    old_password = request.json.get("old_password")
    new_password = request.json.get("new_password")
    # 校验参数
    if not all([old_password,new_password]):
         return jsonify(errno=RET.PARAMERR, errmsg="参数不足")
    if not user:
         return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    # 逻辑处理
    if not user.check_passowrd(old_password):
         return jsonify(errno=RET.PARAMERR, errmsg="旧密码错误 ")

    user.password=new_password
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.PARAMERR, errmsg="密码修改失败")

    return jsonify(errno=RET.OK, errmsg="密码修改成功")




@profile_bp.route("/pic_info",methods=["POST","GET"])
@login_user
def pic_info():
    """用户头像"""
    if request.method == "GET":
        return render_template("news/user_pic_info.html")

    avatar_data = request.files.get("avatar").read()
    user =g.user

    if not avatar_data:
         return jsonify(errno=RET.NODATA, errmsg="图片数据不能为空")
    if not user:
         return jsonify(errno=RET.SESSIONERR, errmsg="用户无法登录")

    try:
        image_name = qiniu_image_store(avatar_data)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="上传图片失败")
    user.avatar_url = image_name

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存用户url失败")
    full_url = constants.QINIU_DOMIN_PREFIX + user.avatar_url
    data = {
        "avatar_url":full_url
    }
    return jsonify(errno=RET.OK, errmsg="上传成功",data=data)


@profile_bp.route("/base_info",methods=["POST","GET"])
@login_user
def base_info():
    """获取用户基本资料页面 修改用户基本资料页面"""
    user = g.user
    if request.method == "GET":
        data = {
            "user_info": user.to_dict() if user else None
        }
        return render_template("news/user_base_info.html" ,data =data)
    else:
        """
        # 1.获取参数
        # 2.校验参数
        # 3.逻辑处理
        # 4.返回值处理
        """
        # 1.获取参数
        params_dict = request.json
        nick_name = params_dict.get("nick_name")
        signature = params_dict.get("signature")
        gender = params_dict.get("gender")

        # 2.校验参数
        if not all([nick_name,signature,gender]):
             return jsonify(errno=RET.PARAMERR, errmsg="参数不足")
        # 判断是否登录
        if not user:
             return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
        if gender not in ["MAN","WOMAN"]:
             return jsonify(errno=RET.PARAMERR, errmsg="gender 数据填写错误")
        # 3.逻辑处理
        user.gender=gender
        user.nick_name = nick_name
        user.signature = signature
        # 更新数据库用户登录信息
        session["nick_name"] = nick_name
        try:
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="修改数据异常")
        # 4.返回值处理
        return jsonify(errno=RET.OK, errmsg="修改用户数据成功")



@profile_bp.route("/info")
@login_user
def user_info():
    """个人用户页面"""
    user = g.user
    data = {
        "user_info":user.to_dict()if user else None
    }
    return render_template("news/user.html" ,data=data)