from  flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template, g
from redis import StrictRedis
from flask_wtf import CSRFProtect
from flask_session import Session
from config import config_dict
import logging
from logging.handlers import RotatingFileHandler
from flask_wtf.csrf import generate_csrf


def creat_log(config_name):
    """记录日志的配置信息"""
    # 设置日志的记录等级
    logging.basicConfig(level=config_dict[config_name].LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)

db = SQLAlchemy()
redis_store=None#type:StrictRedis
def creat_app(config_name):
    """工厂方法，根据传入的不同信息 创建步同的app"""
    # 创建app对象
    creat_log(config_name)
    app = Flask(__name__)
    # 将信息提交到app
    config_class = config_dict[config_name]
    # 将配置注册到app中
    app.config.from_object(config_class)
    # 创建数据库
    db.init_app(app)
    # 创建redis数据库对象
    global redis_store
    redis_store = StrictRedis(host=config_class.REDIS_HOST, port=config_class.REDIS_PORT, db=config_class.REDIS_NUM,
                              decode_responses=True)
    # 开启csrf 保护机制 开启后端验证
    csrf = CSRFProtect(app)
    # 借助第三方调整Session的储存地址
    Session(app)
    # 注册蓝图
    @app.after_request
    def after_request(response):
        csrf_token = generate_csrf()
        response.set_cookie("csrf_token",csrf_token)
        return response

    # 捕获404
    from info.utils.common import login_user
    @app.errorhandler(404)
    @login_user
    def page_not_found(e):
        """页面找不到引导到同意界面"""
        user = g.user
        return render_template("news/404.html",data = {"user_info":user.to_dict()if user else None})

    from info.moudile.admin import admin_dp
    app.register_blueprint(admin_dp)

    from info.utils.common import do_index_class
    app.add_template_filter(do_index_class,"index_class")

    from info.moudile.proflie import profile_bp
    app.register_blueprint(profile_bp)

    from info.moudile.news import news_dp
    app.register_blueprint(news_dp)
    from info.moudile.index import index_dp
    app.register_blueprint(index_dp)
    from info.moudile.passport import passport_bp
    app.register_blueprint(passport_bp)
    return app

