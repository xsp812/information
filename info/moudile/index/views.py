import logging
from flask import session, current_app, render_template, request, jsonify, g
from info.models import User, News, Category
from info.utils.response_code import RET
from . import index_dp
from info import redis_store, constants
from info import models
from info.utils.common import login_user


@index_dp.route("/news_list")
def news_list():
    """新闻首页显示"""
    """
    1.获取参数
    2.校验参数
    3.逻辑处理
    4.返回值处理
    
    """
#     获取参数
    params_dict= request.args
    page = params_dict.get("page",1)
    per_page = params_dict.get("per_page",constants.HOME_PAGE_MAX_NEWS)
    category_id = params_dict.get("cid",)
#     校验参数
    try:
        page =int(page)
        per_page = int(per_page)
        category_id = int(category_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
#     逻辑处理
    filters = [News.status == 0]
    if category_id !=1:
        filters.append(News.category_id==category_id)
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page, False)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库异常")
    items = paginate.items
    total_page = paginate.pages
    current_page = paginate.page

    news_list = []
    for news in items if items else []:
        news_list.append(news.to_dict())

    data = {
        "newsList":news_list,
        "current_page":current_page,
        "total_page":total_page
    }

    return jsonify(errno=RET.OK, errmsg="查询新闻列表成功" ,data =data)



# 127.0.0.1:5000/
@index_dp.route("/")
@login_user
def index():
    # 通过session去获取用户
    user = g.user

    # -------获取点击排行数据
    news_list =[]
    try:
        news_list=News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    click_news_list = []
    for news in news_list if news_list else []:
        click_news_list.append(news.to_dict())

    # ----新闻分类---

    # 获取新闻分分类数据
    categories = Category.query.all()

    categories_dict=[]
    for category in categories if categories else []:
        categories_dict.append(category.to_dict())


    data= {
        "user_info":user.to_dict() if user else None ,
        "click_news_list":click_news_list,
        "categories":categories_dict
    }
    # 没有调整之前 session数据存储到服务器中
    # redis_store.set("name","wang")
    # session["name"]="James"
    # logging.debug("debug 日志信息")
    # logging.info("info日志信息")
    # logging.warning("waring 日志信息")
    # logging.error("error 日志信息")
    # current_app.logger.debug("rizhi")

    return render_template("index.html",data =data)


@index_dp.route("/favicon.ico")
def favicon():
    """返回图标"""
    return current_app.send_static_file("news/favicon.ico")