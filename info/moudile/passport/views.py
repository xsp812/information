import datetime
from . import passport_bp
from flask import request, abort, make_response, jsonify, current_app, session
from info.utils.captcha.captcha import captcha
from info import redis_store, constants, db
import json
from info.utils.response_code import RET
import re
from info.models import User
import random
from info.lib.yuntongxun.sms import CCP

@passport_bp.route("/logout",methods=["POST"])
def logout():
    """退出"""
    """清除session中的数据会退出状态"""
    session.pop("user_id","")
    session.pop("nick_name","")
    session.pop("mobile","")
    session.pop("is_admin",None)

    return jsonify(errno=RET.OK, errmsg="ok")

@passport_bp.route('/login',methods=["POST"])
def login():
    """登录接口"""
    """
    1.获取参数
        1.1 手机号码 密码（未加密的）
    2.校验参数
        2.1 非空判断
        2.2 手机号码格式
    3.逻辑处理
        3.1 根据手机号码查询用户对象
        3.2 对比用户填写的密码和用户对象中密码是否一致
        3.3 一致：记录最后一次登录时间 使用session记录用户登录信息
    4.返回值处理
    """

    # 1.1手机号码密码（未加密的）
    params_dict = request.json
    mobile = params_dict.get("mobile")
    password = params_dict.get("password")
    # 2.1 非空判断
    if not all([mobile, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不足")
    # 2.2 手机号码格式判断
    if not re.match('^1[356789][0-9]{9}$', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机格式错误")

    # 3.1根据手机号码查询用户对象
    try:
        user = User.query.filter_by(mobile = mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询用户对象异常")
    if not user:
         return jsonify(errno=RET.NODATA, errmsg="用户不存在")

    # 3.2对比用户填写的密码和用户对象中密码是否一致
    if not user.check_passowrd(password):
         return jsonify(errno=RET.DATAERR, errmsg="密码输入错误")

    # 3.3一致：记录最后一次登录时间使用session记录用户登录信息
    if user.is_admin==False:
        session["is_admin"]=False
    session["user_id"]= user.id
    session["mobile"]= user.mobile
    session["nick_name"] = user.nick_name

    user.last_login=datetime.datetime.now()


#   当修改了模型身上的属性时候 不需要add只需要直接添加即可
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errmsg="用户对象保存到数据库异常")

    return jsonify(errno=RET.OK, errmsg="登录成功")


@passport_bp.route('/register',methods=["POST"])
def register():
    """用户注册接口"""
    """
    1.获取参数
        1.1手机号码 用户填写的短信验证码 密码（未加密）
    2.校验参数
        2.1 非空判断
        2.2 手机号码格式判断
    3.逻辑处理
        3.1通过手机号码拼接kEY去获取redis中的真是验证码
        3.2对比用户添加的短信验证码和真实的短信验证码对比
        3.3一致：将手机号码保存到数据库
        3.4 用户注册成功后 第一次都会给用户自动登录 使用session存储用户信息
    4.返回值处理
    """
    # 1.1手机号码用户填写的短信验证码密码（未加密）
    params_dict = request.json
    mobile = params_dict.get("mobile")
    smscode = params_dict.get("smscode")
    password = params_dict.get("password")

    # 2.1非空判断
    if not all([mobile,smscode,password]):
         return jsonify(errno=RET.DATAERR, errmsg="参数不足")
    # 2.2手机号码格式判断
    if not re.match('^1[356789][0-9]{9}$',mobile):
         return jsonify(errno=RET.PARAMERR, errmsg="手机格式错误")

    # 3.1通过手机号码拼接kEY去获取redis中的真是验证码
    try:
        real_smscode = redis_store.get("SMS_%s" % mobile)
        if real_smscode:
            redis_store.delete("SMS_%s" % mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询短信验证码错误验证码")

    if not real_smscode:
         return jsonify(errno=RET.NODATA, errmsg="短信验证码过期")

    # 3.2对比用户添加的短信验证码和真实的短信验证码对比
    if smscode != real_smscode:
         return jsonify(errno=RET.PARAMERR, errmsg="短信验证码填写错误")
#     成功创建用户对象
    user = User()
    user.mobile=mobile
    user.nick_name = mobile
#     密码需要加密
    user.password = password
    user.last_login = datetime.datetime.now()
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存用户失败")

    # 3.4用户注册成功后第一次都会给用户自动登录使用session存储用户信息
    session["user_id"] = user.id
    session["nick_name"] = user.nick_name
    session["mobile"] = user.mobile

    # 4.返回值处理
    return jsonify(errno=RET.OK, errmsg="注册成功")

@passport_bp.route("/image_code")
def get_imagecode():
    """获取验证码图片"""
    # 1.获取参数
    # 1.1获取前端传上来的随机UUID编码
    imageCodeId = request.args.get("imageCodeId")
    # 2.校验参数
    # 2.2 判断UUID参数是否为空
    if not imageCodeId:
        abort(404)
    # 3.逻辑处理
    # 3.1生成验证图片对象 验证码的真实值
    name, text, image = captcha.generate_captcha()
    # 3.2 将图片真实值使用偏码存储到redis
    try:
        redis_store.set("imagecode_%s" % imageCodeId, text, ex=constants.IMAGE_CODE_REDIS_EXPIRES)
    except  Exception as e:
        abort(500)
    # 4.返回值处理
    # 创建响应对象
    response = make_response(image)
    # 设置响应头的返回值的类型
    response.headers["Content-Type"] = "image/jpeg"
    return response


@passport_bp.route("/sms_code", methods=["post"])
def send_sms():
    """点击发送短信验证操作"""
    """
    1.获取参数
        1.1手机号码 用户填写的验证码值 UUID随机编号
    2.校验参数
        2.1各个参数是否存在
        2.2手机格式是否正确
    3.逻辑处理
        3.1根据image_code_id编号取redis中获取验证码的值
        3.2删除redis数据库中的值
        3.3没没有值 代码redis中验证码值过期 调用前段字生成一张图片
        3.4比较用户填写的图片验证码和后端真实验证码是否存在
            成功发送短信
            失败 验证码填写错误
        3.5 根据手机号码查询手机号码是否注册过
        3.6调用云通讯发送短信
            生成一个6为的随机短信内容
        3.7发送短信验证码成功
        3.8 存储短信验证码redis
    4.返回值处理
        告知前端发送短信验证码成功
        
    
    """
    # 1.获取参数
    #     1.1手机号码 用户填写的验证码值 UUID随机编号
    param_dict = request.json
    mobile = param_dict.get("mobile")
    image_code = param_dict.get("image_code")
    image_code_id = param_dict.get("image_code_id")
    # 2.校验参数
    #     2.1各个参数是否存在
    if not all([mobile, image_code, image_code_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不足")

    # 手机格式是否正确
    if not re.match("^1[356789][0-9]{9}$", mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机格式错误")
    try:
        # 根据image_code_id编号取redis中获取验证码的值
        real_image_code = redis_store.get("imagecode_%s" % image_code_id)

        # 删除redis数据库中的值
        if real_image_code:
            redis_store.delete("imagecode_%s" % image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询验证码真实值异常")

    # 没有值
    # 代码redis中验证码值过 调用前段字生成一张图片
    if not real_image_code:
        return jsonify(errno=RET.NODATA, errmsg="验证码过期了")

    # 比较用户填写的图片验证码和后端真实验证码是否存在
    if real_image_code.lower() != image_code.lower():
        return jsonify(errno=RET.DATAERR, errmsg="验证码填写错误")

    try:
        # 根据手机号码查询手机号码是否注册过
        user = User.query.filter_by(mobile=mobile).first()
        # 有值表示手机号码已经被注册了
        if user:
            return jsonify(errno=RET.DATAERR, errmsg="手机号已经被注册")
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errmsg="查询对象异常")

    # 调用云通讯发送短信
    # 生成一个6为的随机短信内容
    sms_code = random.randint(0, 99999)
    sms_code = "%06d" % sms_code
    result = CCP().send_template_sms(mobile, [sms_code, constants.IMAGE_CODE_REDIS_EXPIRES / 60], 1)
    if result != 0:
        return jsonify(errno=RET.THIRDERR, errmsg="发送失败")
    # 存储短信验证码redis
    try:
        redis_store.set("SMS_%s" % mobile, sms_code, ex=constants.SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="保存短信验证码异常")
    # 返回值处理 告知前端发送短信验证成功请查收
    return jsonify(errno=RET.OK, errmsg="发送短信验证码成功")
