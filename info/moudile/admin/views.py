import time
from info.utils.image_store import qiniu_image_store
from datetime import datetime, timedelta
from flask import request, g, render_template, current_app, session, redirect, url_for, jsonify

from info import constants, db
from info.models import User, News, Category
from info.utils.common import login_user
from info.utils.response_code import RET
from . import admin_dp

@admin_dp.route('/add_category', methods=["POST"])
def add_category():
    """修改或者添加分类"""

    category_id = request.json.get("id")
    category_name = request.json.get("name")
    if not category_name:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    # 判断是否有分类id
    if category_id:
        try:
            category = Category.query.get(category_id)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="查询数据失败")

        if not category:
            return jsonify(errno=RET.NODATA, errmsg="未查询到分类信息")

        category.name = category_name
    else:
        # 如果没有分类id，则是添加分类
        category = Category()
        category.name = category_name
        db.session.add(category)

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存数据失败")
    return jsonify(errno=RET.OK, errmsg="保存数据成功")



@admin_dp.route('/news_category')
def get_news_category():
    """新闻分类管理"""
    categories = Category.query.all()
    categories_dict_list = []
    for category in categories:
        category_dict = category.to_dict()
        categories_dict_list.append(category_dict)
    # 删除最新分类
    categories_dict_list.pop(0)
    data= {
        "categories":categories_dict_list
    }
    return render_template("admin/news_type.html",data = data)

@admin_dp.route('/news_edit_detail', methods=["POST", "GET"])
def news_edit_detail():
    """新闻编辑修改接口"""
    if request.method == "GET":
        news_id = request.args.get("news_id")

        #查询新闻
        news=None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)

        news_dict = news.to_dict() if news else None

        # 查询所有分类
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
        # 移除最新分类
        categories.pop(0)
        category_dict_list = []
        for category in categories if categories else []:
            # 分类对象转换成字典
            category_dict = category.to_dict()
            # 添加is_selected=False不选择
            category_dict["is_selected"] = False
            # 6 == 6
            # is_selected=True选择中category对应的分类
            if news.category_id == category.id:
                category_dict["is_selected"] = True
            category_dict_list.append(category_dict)

        data = {
            "news": news_dict,
            "categories": category_dict_list
        }

        return render_template("admin/news_edit_detail.html", data=data)

    # POST请求新闻修改
    #1.获取参数
    news_id = request.form.get("news_id")
    title = request.form.get("title")
    digest = request.form.get("digest")
    content = request.form.get("content")
    index_image = request.files.get("index_image")
    category_id = request.form.get("category_id")
    #2.校验参数
    # 1.1 判断数据是否有值
    if not all([title, digest, content, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数有误")

    news = None # type:News
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到新闻数据")

    #3.逻辑处理
    if index_image:
        # 转换成二进制数据
        index_image_data = index_image.read()
        try:
            image_name = qiniu_image_store(index_image_data)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.THIRDERR, errmsg="主图片保存到七牛云失败")
        # 有修改图片才保存
        news.index_image_url = constants.QINIU_DOMIN_PREFIX + image_name

    #修改新闻对象
    news.title = title
    news.category_id = category_id
    news.digest = digest
    news.content = content

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="修改新闻对象失败")
    #4.返回值
    return jsonify(errno=RET.OK, errmsg="新闻编辑成功")


@admin_dp.route('/news_edit')
def news_edit():
    """返回新闻列表"""

    page = request.args.get("p", 1)
    keywords = request.args.get("keywords", "")
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    news_list = []
    current_page = 1
    total_page = 1

    try:
        filters = []
        # 如果有关键词
        if keywords:
            # 添加关键词的检索选项
            filters.append(News.title.contains(keywords))

        # 查询
        paginate = News.query.filter(*filters) \
            .order_by(News.create_time.desc()) \
            .paginate(page, constants.ADMIN_NEWS_PAGE_MAX_COUNT, False)

        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_list = []
    for news in news_list:
        news_dict_list.append(news.to_basic_dict())

    context = {"total_page": total_page, "current_page": current_page, "news_list": news_dict_list}

    return render_template('admin/news_edit.html', data=context)


@admin_dp.route("/news_review_detail",methods=["POST","GET"])
def news_review_detail():
    """新闻审核页面详情显示"""
    if request.method == "GET":
        news_id = request.args.get("news_id")
        news=None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)
        if not news:
            return render_template("admin/news_review_detail.html",errmsg="没有该新闻")
        data = {
            "news":news.to_dict()
        }

        return render_template("admin/news_review_detail.html",data=data)

    news_id = request.json.get("news_id")
    action = request.json.get("action")
    # 校验参数
    if not all([news_id,action]):
         return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    if action not in ("accept", "reject"):
         return jsonify(errno=RET.PARAMERR, errmsg="action参数错误")
    # 逻辑处理
    news=None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
    if not news:
         return jsonify(errno=RET.NODATA, errmsg="没有该新闻")

    if action == "accept":
        news.status = 0
    else:
        reason = request.json.get("reason")
        if not reason:
            return jsonify(errno=RET.PARAMERR, errmsg="请填写拒绝原因")
        news.reason = reason
        news.status = -1
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="提交数据库异常")
    return jsonify(errno=RET.OK, errmsg="OK")


@admin_dp.route("news_review")
def news_review():
    """返回待审核新闻列表"""
    p = request.args.get("p",1)
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p =1

    news_list =[]
    current_page = 1
    total_page = 1
    filter=[News.status !=0 ]
    try:
        paginate=News.query.filter(*filter).order_by(News.create_time.desc()).paginate(p,constants.ADMIN_NEWS_PAGE_MAX_COUNT,False)
        news_list=paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_list=[]
    # 将新闻转化为字典列表
    for news in news_list if news_list else []:
        news_dict_list.append(news.to_dict())
    data = {
        "current_page":current_page,
        "total_page":total_page,
        "news_list":news_dict_list
    }
    return render_template("admin/news_review.html",data=data)

@admin_dp.route('/user_list')
@login_user
def user_list():
    """用户列表展示"""
    #1.获取参数
    p = request.args.get("p" ,1)
    user = g.user
    try:
        p= int(p)
    except Exception as e:
        current_app.logger.error(e)
        p =1
    user_list = []
    current_page = []
    total_page= []
    if user:
        try:
            paginate = User.query.filter(User.is_admin == False).\
               paginate(p, constants.ADMIN_USER_PAGE_MAX_COUNT, False)
            user_list = paginate.items
            current_page = paginate.page
            total_page = paginate.pages
        except Exception as e:
            current_app.logger.error(e)
    user_dict_list = []
    for user in user_list if user_list else []:
        user_dict_list.append(user.to_admin_dict())

    data = {
        "users":user_dict_list,
        "current_page" : current_page,
        "total_page":total_page
    }
#     返回值
    return render_template("admin/user_list.html",data= data)



@admin_dp.route('/user_count')
def user_count():
# 查询总人数
    total_count = 0
    try:
        # 10001
        total_count = User.query.filter(User.is_admin == False).count()
    except Exception as e:
        current_app.logger.error(e)

    # 查询月新增数
    mon_count = 0
    try:
        # 2018-8-31号
        now = time.localtime()
        # 2018-08-01月初  2018-09-01
        mon_begin = '%d-%02d-01' % (now.tm_year, now.tm_mon)
        # 使用strptime将字符串转成时间date格式 '%Y-%m-%d'
        mon_begin_date = datetime.strptime(mon_begin, '%Y-%m-%d')
        # 用户的创建时间 > 每一个月的第一天 -->每一个月的新增人数
        mon_count = User.query.filter(User.is_admin == False, User.create_time >= mon_begin_date).count()
    except Exception as e:
        current_app.logger.error(e)

    # 查询日新增数
    day_count = 0
    try:
        # 2018-08-31-0:0
        day_begin = '%d-%02d-%02d' % (now.tm_year, now.tm_mon, now.tm_mday)
        day_begin_date = datetime.strptime(day_begin, '%Y-%m-%d')
        # 当前时间 > 2018-08-31-0:0(一天的开始时间)
        day_count = User.query.filter(User.is_admin == False, User.create_time > day_begin_date).count()
    except Exception as e:
        current_app.logger.error(e)

    # 查询图表信息
    # 获取到当天00:00:00时间
    # 2018-08-31
    now_date = datetime.strptime(datetime.now().strftime('%Y-%m-%d'), '%Y-%m-%d')
    # 定义空数组，保存数据
    active_date = []
    active_count = []

    # 依次添加数据，再反转
    for i in range(0, 31):
        # 2018-08-31 - 1  =  2018-08-30:0:0
        # 2018-08-31 - 2  =  2018-08-29:0:0
        begin_date = now_date - timedelta(days=i)
        #  now_date - timedelta(days=(i))  + timedelta(days=(1))
        #  begin_date + timedelta(days=(1))
        # 2018-08-30:24:00
        # 2018-08-29:24:00
        end_date = begin_date + timedelta(days=1)
        # 记录这个月的每一天  30 29 ....01
        #strftime将date格式转换成字符串
        active_date.append(begin_date.strftime('%Y-%m-%d'))
        count = 0
        try:
            count = User.query.filter(User.is_admin == False, User.last_login >= begin_date,
                                      User.last_login < end_date).count()
        except Exception as e:
            current_app.logger.error(e)
        # 记录一个月的每天的用户活跃数
        active_count.append(count)

    # 日期反转
    active_date.reverse()
    # 数据反转
    active_count.reverse()

    data = {"total_count": total_count, "mon_count": mon_count, "day_count": day_count, "active_date": active_date,
            "active_count": active_count}

    return render_template('admin/user_count.html', data=data)

@admin_dp.route('/index',methods= ["GET","POST"])
@login_user
def admin_index():
    """"""
    user = g.user
    return render_template("admin/index.html",user = user.to_dict())


@admin_dp.route('/login',methods= ["GET","POST"])
@login_user
def admin_login():
    """管理员用户登录"""
    if request.method == "GET":
        user_id = session.get("user_id",None)
        is_admin = session.get("is_admin",False)
        if user_id and is_admin:
            return redirect(url_for("admin.admin_index"))
        return render_template("admin/login.html")
    # 1.获取参数

    username = request.form.get("username")
    password = request.form.get("password")

    # 校验参数
    if not all([username,password]):
        return render_template('admin/login.html',errmsg ="参数不足")
    try:
        user = User.query.filter(User.mobile==username).first()
    except Exception as e:
        current_app.logger.error(e)
        return render_template("admin/login.html",errmsg = "数据库查询失败")
    if not user:
        return render_template('admin/login.html', errmsg="用户不存在")
    if not user.check_passowrd(password):
        return render_template('admin/login.html', errmsg="密码错误")
    if not user.is_admin:
        return render_template("admin/login.html",errmsg="你不是管理员用户")

    session["user_id"] = user.id
    session["nick_name"] = user.nick_name
    session["mobile"] = user.mobile
    session["is_admin"] = True

    # TODO 跳转到后台管理主页,暂未实现
    return redirect(url_for("admin.admin_index"))