from redis import StrictRedis
import logging

class Config(object):
    """项目配置类"""
    DEBUG=True
    # mysql数据库的配置信息
    SQLALCHEMY_DATABASE_URI= "mysql://root:mysql@127.0.0.1:3306/information"
    # 开启数据库跟踪
    SQLALCHEMY_TRACK_MODIFICATIONS= True
    # redis 数据库的配置信息
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379
    REDIS_NUM =6

    SECRET_KEY = "DSFASDFSFASDFDSAFADSFASDFEWREWREWREWREWREW"
    # 设置数据库存储类型
    SESSION_TYPE= "redis"
    # 创建数据库实例对象
    SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_PORT,db=REDIS_NUM)

    # 要加密处理

    SESSION_USE_SIGNER=True
    #  设置过期时长
    SESSION_PERMANENT = False
    # 设置过期时长
    PERMANENT_SESSION_LIFETIME=86400 * 2


class DevelopmentConfig(Config):
    DEBUG = True
    #设置开发环境日志级别
    LOG_LEVEL = logging.DEBUG

class ProuctionConfig(Config):
    DEBUG = False
    # 设置线上环境日志级别
    LOG_LEVEL = logging.WARNING
# 提供一个接口给外界调用
config_dict={
    "development":DevelopmentConfig,
    "prouction":ProuctionConfig
}