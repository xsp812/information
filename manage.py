from flask import current_app
from flask_script import Manager
from info import creat_app,db
from flask_migrate import Migrate,MigrateCommand


# 调用工厂方式创建app
from info.models import User

app = creat_app("development")
# 创建管理类
manager = Manager(app)
# 创建迁移对象
Migrate(app,db)
# 添加迁移命令
manager.add_command("db",MigrateCommand)


@manager.option('-n', '-name', dest='name')
@manager.option('-p', '-password', dest='password')
def createsuperuser(name , password):
    """创建管理员用户"""
    if not all([name,password]):
        return "参数不足"
    user = User()
    user.mobile = name
    user.nick_name =name
    user.password=password
    user.is_admin = True

    try:
        db.session.add(user)
        db.session.commit()
        print("创建管理员成功")
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()

if __name__ == '__main__':
    # python manager runserve -h -p -d
    manager.run()

